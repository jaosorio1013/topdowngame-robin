# TopDownGame Robin

🇪🇸️ Repositorio del proyecto Top Down Game de Godot Engine, con Pixel Art de Ansimuz.

🇬🇧️ Repository of the Top Down Game project of Godot Engine, with Pixel Art by Ansimuz.

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://github.com/alainm23/planner/blob/master/LICENSE)
[![Donate](https://img.shields.io/badge/PayPal-Donate-gray.svg?style=flat&logo=paypal&colorA=0071bb&logoColor=fff)]("https://paypal.me/joseleon1971")

<div align="center">
  <span align="center"> <img width="80" height="90" src="https://pbs.twimg.com/profile_images/1211845531020021760/x3NlGNpu_400x400.png" alt="Icon"></span>
  <h1 align="center">Top Down Game for Godot Engine - Ansimuz Pixel Art</h1>
</div>

## Instalation

You will need:

* [Godot Engine 3.2 or Godot Engine 3.1](https://godotengine.org/download/linux)

## Credits

* Pixel Art:
    * [Ansimuz](https://ansimuz.itch.io/) Top Down Game assetspack.
* GDScript:
    * [Securas](https://securas.itch.io/) Finite State Machine.

Thank you very much to all these great people for their contributions to this project.

## Support

You can help me continue with the development of assets and demonstrations if you buy this resource at itch.io or send a donation by PayPal or Patreon.
* [PayPal](https://paypal.me/joseleon1971).
* [Patreon](https://www.patreon.com/indielibre).
* [Itch.io](https://in3mo.itch.io/).

Visit [indielibre.com](https://indielibre.com/) for tutorials on these resources or extra information.