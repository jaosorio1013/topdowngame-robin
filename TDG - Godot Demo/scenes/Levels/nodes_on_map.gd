extends TileMap

var trees := {
	0: "res://actors/tree/Tree.tscn"
};

func _ready() -> void:
	make_trees();


func make_trees() -> void:
	for objects_int in trees.size():
		for c in get_used_cells_by_id(objects_int):
			new_instance(trees[trees.keys()[objects_int]], c);


func new_instance(item, pos) -> void:
	var new_item = load(item).instance();
	add_child(new_item);
	new_item.owner = owner;
	new_item.global_position = map_to_world(pos);
	set_cellv(pos, -1);
