extends Node2D

var robin = load("res://actors/characters/hero/Hero.tscn");

export var map_dir: NodePath;
onready var map: TileMap = get_node(map_dir);
onready var start_position = $start_position.global_position;

export var scene1: String;
export var current_scene_dir: String;

func _ready() -> void:
	if !owner:
		return
	
	instance_player();


func configure_camera(scene) -> void:
	var result: Vector2;
	
	for x in map.get_used_cells():
		if x.y != 0:
			break;
		result.x = x.x-1.5;
	
	scene.camera.limit_right = result.x*16;
	scene.camera.limit_left = 0;
	scene.camera.limit_top = 0;
	scene.camera.limit_bottom = map.get_used_cells()[map.get_used_cells().size() - 1].y * 16;


func instance_player() -> void:
	var new_player = robin.instance();
	$TileMaps/Nodes.add_child(new_player);
	new_player.global_position = start_position;
	
	# Configure Camera for the Player
	configure_camera(new_player);


func _on_next_scene_body_entered(body) -> void:
	owner.transitions.next_scene(scene1);
	owner.transitions.last_scene_dir = current_scene_dir;
	owner.transitions.load_state();
