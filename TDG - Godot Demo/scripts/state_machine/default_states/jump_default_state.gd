extends "res://scripts/state.gd"

var current_jump: int;


func enter() -> void:
	current_jump = 0;
	anim_character.play(self_anim_name);
	jump();


func jump() -> void:
	character.velocity.y = -character.jump_height;
	character.move_and_slide(character.velocity,Vector2(0,-1));
	current_jump += 1;


func handle_input(event) -> void:
	if Input.is_action_just_pressed("ui_up"):
		if character.max_jumps > current_jump:
			jump();


func update(delta) -> void:
	get_direction();
	
	if character.is_on_floor():
		parent.change_state("Idle");
	
	move(delta);