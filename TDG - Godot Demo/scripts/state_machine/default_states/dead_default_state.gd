extends "res://scripts/state_machine/state.gd"


func enter() -> void:
	parent.set_active(false);
	anim_character.play(self_anim_name);
	anim_character.connect("animation_finished", self, "dead");


func dead(anim_name) -> void:
	if anim_name == self_anim_name:
		character.set_physics_process(false);
		character.set_process(false);
		character.set_process_input(false);
		anim_character.disconnect("animation_finished", self, "dead");
