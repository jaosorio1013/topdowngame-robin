extends Node

onready var parent: Node = get_parent(); # StateMachine is the parent;
var character: KinematicBody2D = owner; # Player or NPc node is the Owner;

var anim_character: AnimationPlayer = null;
var self_anim_name: String = "";

var speed: float;

var velocity := Vector2();
var direction := Vector2();


func enter() -> void:
	pass


func update(delta) -> void:
	pass


func handle_input(event) -> void:
	pass


func get_direction() -> void:
	if character.is_in_group("Players"):
		direction.x = int(Input.is_action_pressed("ui_right"))-int(Input.is_action_pressed("ui_left"));
		direction.y = int(Input.is_action_pressed("ui_down"))-int(Input.is_action_pressed("ui_up"))
		
		if direction.x and direction.y:
			direction.x = 0;
			direction.y = 0;
		
	elif character.is_in_group("Enemies"):
		direction = character.direction;
	
	if direction.x != 0:
		character.get_node("pivot").scale = Vector2(direction.x, 1);
	
	character.direction = direction;


func move(delta) -> void:
	if character.is_in_group("Players"):
		velocity.x = speed * direction.x;
		velocity.y = speed * direction.y;
		character.move_and_slide(velocity);
	elif character.is_in_group("Enemies"):
		var distance = character.distance * speed * delta;
		velocity.x = (distance * direction.x);
		velocity.y = (distance * direction.y);
		character.move_and_slide(velocity);


func exit() -> void:
	pass