extends Area2D

var speed := 180;
var direction := Vector2();


func _physics_process(delta) -> void:
	if direction.x > 0:
		rotation_degrees = 90;
	elif direction.x < 0:
		rotation_degrees = -90;
	
	if direction.y > 0:
		rotation_degrees = 180;
	elif direction.y < 0:
		rotation_degrees = 0;
	
	global_position.x += direction.x * speed * delta;
	global_position.y += direction.y * speed * delta;


func _on_Arrow_body_entered(body) -> void:
	queue_free();


func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free();


func _on_Arrow_area_entered(area) -> void:
	if area.get_name() == "Body" and area.owner.is_in_group("Enemies"):
		area.owner.state_machine.change_state("Dead");
		self.queue_free();
