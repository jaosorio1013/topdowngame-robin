extends KinematicBody2D

export var lifes: int = 5 setget set_life;
export var max_attacks: int = 3;
export var speed: float = 26.00;
export var jump_height: float = 150;
export var max_jumps: int = 2;

onready var state_machine: Node;
onready var animation_player: AnimationPlayer;

var direction := Vector2();
var velocity := Vector2();


func _physics_process(delta) -> void:
	pass


func set_life(new_life) -> void:
	lifes = new_life;
	
	if lifes <= 0:
		state_machine.change_state("Dead");


func cancel_velocity() -> void:
	velocity = Vector2.ZERO;
