extends "res://actors/characters/Character.gd"

onready var camera = $camera;
onready var bow_pos = $pivot/Bow_pos.global_position;

func _ready() -> void:
	state_machine = $StateMachine;
	animation_player = $Anim;
	
	state_machine.start();
