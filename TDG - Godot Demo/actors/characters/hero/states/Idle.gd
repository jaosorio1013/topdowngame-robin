extends "res://scripts/state_machine/default_states/idle_default_state.gd"


func enter() -> void:
	set_anim_direction();


func update(delta) -> void:
	set_anim_direction();
	.update(delta);


func set_anim_direction() -> void:
	if !direction.x and direction.y == -1:
		anim_character.play("IdleBack");
	elif !direction.x and direction.y == 1:
		anim_character.play("IdleFront");
	elif direction.x and !direction.y:
		anim_character.play("IdleSide");
