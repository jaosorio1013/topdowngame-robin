extends StaticBody2D

var value = false;


func _on_detect_player_body_entered(body: KinematicBody2D) -> void:
	if body is KinematicBody2D:
		$effect.interpolate_property(self, "modulate", Color.white, Color(1, 1, 1, 0.5), 0.1, Tween.TRANS_BACK, Tween.EASE_IN);
		$effect.start();


func _on_detect_player_body_exited(body: KinematicBody2D) -> void:
	if body is KinematicBody2D:
		$effect.interpolate_property(self, "modulate", self.modulate, Color.white, 0.1, Tween.TRANS_BACK, Tween.EASE_IN);
		$effect.start();
